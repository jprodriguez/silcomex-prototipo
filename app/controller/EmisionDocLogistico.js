/**
* 
*/
Ext.define('Exe.silcomex.controller.EmisionDocLogistico', {
    extend: 'Ext.app.Controller',
    refs : [{
        ref: 'contenedoPrincipal',
        selector: '#panelContenedor'
    }],
    
    stores: [
        'emisionDocLogistico.TipoOperacionDocLogisticos',
        'emisionDocLogistico.TipoDocumentoIMO',
        'emisionDocLogistico.TipoEstadoDocumento',
        'emisionDocLogistico.storeDocumentosIMO',
        'emisionDocLogistico.storeShipperConsignorSender',
        'emisionDocLogistico.storeConsignee',
        

    ],
    
    models: [
        'emisionDocLogistico.modelListaDocumentosIMO',
        'emisionDocLogistico.modelListaShipperConsignorSender',
        'emisionDocLogistico.modelListaConsignee',
    ],
    
    views: [
        'emisionDocLogistico.MainEmDocLogistico',
        'emisionDocLogistico.MainCargaPeligrosa',
        'emisionDocLogistico.WinSelTipoDocIMO',
        'emisionDocLogistico.FormCrearDangerousGood'

    ],

    init: function(){
        console.log('Iniciando controller EmisionDocLogistico');
        this.control({
            'button[action=emDocLogisticos]': {
                click : this.initEmDocLogistico
            },
            'button[action=selTipoDocIMO]': {
                click : this.initWinSelTipoDocIMO
            },
            'button[action=crearDangerousGood]': {
                click : this.initFormCrearDangerousGood
            }
        });
    },

    initEmDocLogistico: function(){
        console.log('click Emision de Documentos Logisticos');
        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'mainEmDocLogistico'
        });
        this.getContenedoPrincipal().doLayout();
    },

    initWinSelTipoDocIMO: function(){
        console.log('click Levantar Popup Seleccion de Tipo de Documento IMO');
        var w = Ext.create('Exe.silcomex.view.emisionDocLogistico.WinSelTipoDocIMO');
        w.show();
    },

    initFormCrearDangerousGood: function(){
        console.log('click Crear IMO Dangerous Good');

        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'formCrearDangerousGood'
        });
        this.getContenedoPrincipal().doLayout();
        //var w = winSeleccionTipoDocumento;
        //var w = Ext.create('Exe.silcomex.view.emisionDocLogistico.WinSelTipoDocIMO');
        //w.close();
    }
});

