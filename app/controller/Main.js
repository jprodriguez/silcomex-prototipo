/**
* 
*/
Ext.define('Exe.silcomex.controller.Main', {
    extend: 'Ext.app.Controller',
	refs : [{
    	ref: 'contenedoPrincipal',
    	selector: '#panelContenedor'
    }],
	
	stores: [
		'aventuraMaritima.TipoServicio',
		'aventuraMaritima.Pais',
		'aventuraMaritima.Puerto',
		'aventuraMaritima.ResultadoServicios',
        'storeServicioNavieros',
		'storeCriterio'
    ],
	
	models: [
		'aventuraMaritima.ResultadoServicios',
		'GLOBAL_Combo',
		'modelServicioNavieros'
	],
	
    views: [
		'aventuraMaritima.RegistroServiciosNavieros',
		'aventuraMaritima.BusquedaServiciosNavieros',
        'aventuraMaritima.MainAventuraMaritima',
        'explorarFicheros.MainExplorarFicheros',
        'exportacion.MainExportacion',
        'exportacion.DCLCargaContenerizada',
        'exportacion.DCLCargaFraccionada',        
        'exportacion.InformacionGeneral',
        'exportacion.GridUnidadesTransporte',
        'emisionDocLogistico.MainCargaPeligrosa',
        'emisionDocLogistico.MainEmDocLogistico',
        'gdm.MainGestionDemanda',

    ],

    init: function(){
    	console.log('Iniciando controller Principal');
    	this.control({
    		'menuitem[action=aventuraMaritima]': {
    			click : this.initAventuraMaritima
            },
			'menuitem[action=buscarServicios]': {
    			click : this.initBuscarServicios
            },
            'menuitem[action=explorarFicheros]': {
                click : this.initExplorarFicheros
            },
            'menuitem[action=exportacion]': {
                click : this.initExportacion
            },
            'menuitem[action=emDocLogisticos]': {
                click : this.initEmDocLogistico
            },
            'menuitem[action=gestionDemanda]': {
                click : this.initGestionDemanda
            },    
            'menuitem[action=registrarDCLCargaContenerzada]': {
                click : this.initRegistrarDCLCargaContenerzada
            },
            'menuitem[action=registrarDCLCargaFraccionada]': {
                click : this.initRegistrarDCLCargaFraccionada
            }
    	});
    },

    initAventuraMaritima: function(){
    	console.log('click Aventura Maritima');
    	this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'registroServiciosNavieros'
        });
    	this.getContenedoPrincipal().doLayout();
    },
	
	initBuscarServicios: function(){
		console.log('click Buscar Servicios');
    	this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'busquedaServiciosNavieros'
        });
    	this.getContenedoPrincipal().doLayout();
	},

    initExplorarFicheros: function(){
        console.log('click Explorar Ficheros');
        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'mainExplorarFicheros'
        });
        this.getContenedoPrincipal().doLayout();
    },

    initRegistrarDCLCargaContenerzada: function(){
        console.log('click initRegistrarDCLCargaContenerzada');
        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'DCLCargaContenerizada'
        });
        this.getContenedoPrincipal().doLayout();
    },

    initRegistrarDCLCargaFraccionada: function(){
        console.log('click initRegistrarDCLCargaFraccionada');
        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'DCLCargaFraccionada'
        });
        this.getContenedoPrincipal().doLayout();
    },

    initEmDocLogistico: function(){
        console.log('click Emision de Documentos Logisticos');
        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'mainEmDocLogistico'
        });
        this.getContenedoPrincipal().doLayout();
    },

    initGestionDemanda: function(){
        console.log('click Gestion de la Demanda');
        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'mainGestionDemanda'
        });
        this.getContenedoPrincipal().doLayout();
    }    

});

