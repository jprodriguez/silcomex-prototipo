/**
* 
*/
Ext.define('Exe.silcomex.controller.AventuraMaritima', {
    extend: 'Ext.app.Controller',
    refs : [{
        ref: 'contenedoPrincipal',
        selector: '#panelContenedor'
    }, {
		ref: 'rsv',
		selector: 'registroServiciosNavieros'
	}, {
		ref: 'bsn',
		selector: 'busquedaServiciosNavieros'
	}, {
		ref: 'winNuevoServicio',
		selector: 'nuevoRegistroServiciosNavieros'
	}, {
		ref: 'winAgregarNaviera',
		selector: 'winAgregarNaviera'
	}, {
		ref: 'winCrearRuta',
		selector: 'winCrearRuta'
	}, {
		ref: 'winDefinicionTiempos',
		selector: 'winDefinicionTiempos'
	}, {
		ref: 'detalleServicioNaviero',
		selector: 'detalleServicioNaviero'
	}],
	models:[
		'aventuraMaritima.NavesResultado',
		'aventuraMaritima.Navieras',
		'aventuraMaritima.Rutas',
		'aventuraMaritima.SubRutas',
		'aventuraMaritima.NavesEnServicio',
		'aventuraMaritima.PuertosRuta',
		'aventuraMaritima.ResultadoRutas',
		'aventuraMaritima.TiemposEntrePuertos',
		'aventuraMaritima.PuertosTransbordos',
		'aventuraMaritima.DetalleRutaServicio'
	],
	stores:[
		'aventuraMaritima.NavesResultado',
		'aventuraMaritima.Navieras',
		'aventuraMaritima.Rutas',
		'aventuraMaritima.CriterioBusqueda',
		'aventuraMaritima.Naves',
		'aventuraMaritima.SubRutas',
		'aventuraMaritima.TipoServicio',
		'aventuraMaritima.Transbordos',
		'aventuraMaritima.NavesEnServicio',
		'aventuraMaritima.Destinacion',
		'aventuraMaritima.PuertosRuta',
		'aventuraMaritima.ResultadoBusqueda',
		'aventuraMaritima.TiemposEntrePuertos',
		'aventuraMaritima.PuertosTransbordos',
		'aventuraMaritima.DetalleRutaServicio'
	],
	
	views: [
		'aventuraMaritima.RegistroServiciosNavieros',
		'aventuraMaritima.BusquedaServiciosNavieros',
		'aventuraMaritima.WinAgregarNaviera',
		'aventuraMaritima.WinCrearRuta',
		'aventuraMaritima.WinDefinicionTiempos',
		'aventuraMaritima.WinAgregarTransbordos',
		'aventuraMaritima.NuevoRegistroServiciosNavieros',
		'aventuraMaritima.DetalleServicioNaviero'
    ],
    
    init: function(){
    	console.log('Iniciando controller AventuraMaritima');
		this.control({
    		'button[action=agregarServicio"]': {
    			click : this.initAgregarServicio
            },
            'button[action=eliminarServicio]': {
                click : this.initEliminarServicio
            },
            'button[action=modificarServicio]': {
                click : this.initModificarServicio
            },
			'button[action=agregarNaviera]': {
                click : this.initAgregarNaviera
            },
			'button[action=eliminarNaviera]': {
                click : this.initEliminarNaviera
            },
			'button[action=agregarRuta]': {
                click : this.initAgregarRuta
            },
			'button[action=modificarRuta]': {
                click : this.initModificarRuta
            },
			'button[action=eliminarRuta]': {
                click : this.initEliminarRuta
            },
			'button[action=agregarTransitTime]': {
                click : this.initAgregarTransitTime
            },
			'button[action=agregarPuertoTransbordo]': {
                click : this.initAgregarPuertoTransbordo
            },
			'button[action=cancelarServicio]': {
                click : this.initCancelarServicio
            },
			'button[action=siguientePanelServicio]': {
                click : this.initSiguientePanelServicio
            },
			'button[action=atrasNaviera]': {
                click : this.initAtrasNaviera
            }, 
			'button[action=siguienteNaviera]': {
                click : this.initSiguienteNaviera
            }, 
			'button[action=atrasNaves]': {
                click : this.initAtrasNaves
            }, 
			'button[action=siguienteNaves]': {
                click : this.initSiguienteNaves
            }, 
			'button[action=atrasRutas]': {
                click : this.initAtrasRutas
            }, 
			'button[action=finalizarServicio]': {
                click : this.initFinalizarServicio
            }, 
			'button[action=cancelarWinAgregarNaviera]': {
                click : this.cancelarWinAgregarNaviera
            },
			'button[action=guardarWinAgregarNaviera]': {
                click : this.guardarWinAgregarNaviera
            },
			'button[action=cancelarWinAgregarRuta]': {
                click : this.cancelarWinAgregarRuta
            },
			'button[action=guardarWinAgregarRuta]': {
                click : this.guardarWinAgregarRuta
            },
			'button[action=cancelarWinDefinicionTiempos]': {
                click : this.cancelarWinDefinicionTiempos
            },
			'button[action=guardarWinDefinicionTiempos]': {
                click : this.guardarWinDefinicionTiempos
            },
			'button[action=verDetalleServicio]': {
                click : this.verDetalleServicio
            }
    	});
    	
    },
	
	initAgregarServicio: function(){
    	console.log('click Agregar Servicio');
		var w = Ext.create('Exe.silcomex.view.aventuraMaritima.NuevoRegistroServiciosNavieros');
		w.show();
    },
	
	initAgregarNaviera: function(){
    	console.log('click Agregar Naviera');
		var w = Ext.create('Exe.silcomex.view.aventuraMaritima.WinAgregarNaviera');
		w.show();
    },
	initEliminarNaviera: function() {
		console.log('click Eliminar Naviera');
		var grilla, panel, me = this;
		panel = me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo').getComponent('panelNaviera');
		grilla = panel.getComponent('gridpanelNaviera');
		
		if (grilla.getSelectionModel().getSelection().length === 0) {
			Ext.MessageBox.show({
				title: 'Eliminar.',
				msg: 'Debe seleccionar un elemento de la grilla.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		
		var selection, store = grilla.getStore();
		selection = grilla.getSelectionModel().getSelection();
		
		store.remove(selection);
	},
	
	initAgregarPuertoTransbordo: function () {
		console.log('click Agregar Puerto Transbordo');
		var w = Ext.create('Exe.silcomex.view.aventuraMaritima.WinAgregarNaviera');
		w.show();
	},
	
	initAgregarRuta : function() {
		console.log('click Agregar Ruta');
		var w = Ext.create('Exe.silcomex.view.aventuraMaritima.WinCrearRuta');
		w.show();
	},
	
	initModificarRuta: function () {
		console.log('click Modificar Ruta');
		var ruta, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		ruta = tab.getComponent('panelRutas');
		grilla = ruta.getComponent('gridpanelRuta');
		if (grilla.getSelectionModel().getSelection().length === 0) {
			Ext.MessageBox.show({
				title: 'Modificar.',
				msg: 'Debe seleccionar un elemento de la grilla.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		var win = Ext.create('Exe.silcomex.view.aventuraMaritima.WinCrearRuta');
		win.show();
	},
	
	initEliminarRuta: function(){
		console.log('click Modificar Ruta');
		var ruta, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		ruta = tab.getComponent('panelRutas');
		grilla = ruta.getComponent('gridpanelRuta');
		if (grilla.getSelectionModel().getSelection().length === 0) {
			Ext.MessageBox.show({
				title: 'Modificar.',
				msg: 'Debe seleccionar un elemento de la grilla.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		var selection, store = grilla.getStore();
		selection = grilla.getSelectionModel().getSelection();
		store.remove(selection);
	},
	
	initAgregarTransitTime : function () {
		console.log('click Agregar TransitTime');
		var w = Ext.create('Exe.silcomex.view.aventuraMaritima.WinDefinicionTiempos');
		w.show();
	},
	
	initAgregarPuertoTransbordo : function () {
		console.log('click Agregar Puerto Transbordo');
		var w = Ext.create('Exe.silcomex.view.aventuraMaritima.WinAgregarTransbordos');
		w.show();
	},

    initEliminarServicio: function(){
        console.log('click Eliminar Servicios');
    },

    initModificarServicio: function(){
		var grilla, panel, me = this;
		grilla = me.getRsv().getComponent('fresultado').getComponent('grillaResultadoServicios');
		if (grilla.getSelectionModel().getSelection().length === 0) {
			Ext.MessageBox.show({
				title: 'Modificar.',
				msg: 'Debe seleccionar un elemento de la grilla.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		var w = Ext.create('Exe.silcomex.view.aventuraMaritima.NuevoRegistroServiciosNavieros');
		w.show();
    },
	
	initCancelarServicio : function () {
		var win, me = this;
		
		win = me.getWinNuevoServicio();
		Ext.MessageBox.show({
			title: 'Cancelar.',
			msg: '¿Está seguro de cancelar?, se perderán todos los datos.',
			buttons: Ext.MessageBox.OKCANCEL,
			fn : cancelar,
			icon: Ext.MessageBox.QUESTION
		});
		function cancelar(btn, l, msg) {
			console.log('cancela');
			if (btn === 'ok') {
				win.close();
			}
		};
	},
	initSiguientePanelServicio: function () {;
		var servicio, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		servicio = tab.getComponent('panelServicio');
		if (!servicio.getForm().isValid()) {
			Ext.MessageBox.show({
				title: 'Servicio Siguiente.',
				msg: 'Verifique datos del formulario.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		tab.setActiveTab('panelNaviera');
	},
	
	initAtrasNaviera: function() {
		var naviera, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		naviera = tab.getComponent('panelNaviera');
		tab.setActiveTab('panelServicio');
	},
	
	initSiguienteNaviera: function() {
		var naviera, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		naviera = tab.getComponent('panelNaviera');
		tab.setActiveTab('panelNaves');
	},
	
	initAtrasNaves:function () {
		var nave, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		nave = tab.getComponent('panelNaves');
		tab.setActiveTab('panelNaviera');
	},
	
	initSiguienteNaves: function () {
		var nave, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		nave = tab.getComponent('panelNaves');
		tab.setActiveTab('panelRutas');
	},
	
	initAtrasRutas: function() {
		var ruta, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		ruta = tab.getComponent('panelRutas');
		tab.setActiveTab('panelNaves');
	},

	initFinalizarServicio: function () {
		var win, servicio, navieras, naves, ruta, tab, me = this;
		tab =  me.getWinNuevoServicio().getComponent('fieldsetNuevo').getComponent('tabpanelNuevo');
		servicio = tab.getComponent('panelServicio');
		navieras = tab.getComponent('panelNaviera');
		naves = tab.getComponent('panelNaves');
		ruta = tab.getComponent('panelRutas');
		win = me.getWinNuevoServicio();
		win.close();
	},
	
	cancelarWinAgregarNaviera: function(){
		var me = this;
		win = me.getWinAgregarNaviera();
		win.close();
	},
	
	guardarWinAgregarNaviera: function () {
		var me = this;
		win = me.getWinAgregarNaviera();
		form = win.getComponent('formAgregarNaviera');
		if (!form.isValid()) {
			Ext.MessageBox.show({
				title: 'Agregar Naviera.',
				msg: 'Verifique datos del formulario.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		
		win.close();
	},
	
	cancelarWinAgregarRuta: function(){
		var win, me = this;
		win = me.getWinCrearRuta();
		win.close();
	},
	
	guardarWinAgregarRuta:function () {
		var win, me = this;
		win = me.getWinCrearRuta();
		form = win.getComponent('formAgregarRuta');
		if (!form.isValid()) {
			Ext.MessageBox.show({
				title: 'Agregar Naviera.',
				msg: 'Verifique datos del formulario.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		
		win.close();
	},
	
	cancelarWinDefinicionTiempos: function () {
		var win, me = this;
		win = me.getWinDefinicionTiempos();
		Ext.MessageBox.show({
			title: 'Cancelar.',
			msg: '¿Está seguro de cancelar?, se perderán todos los datos.',
			buttons: Ext.MessageBox.OKCANCEL,
			fn : cancelar,
			icon: Ext.MessageBox.QUESTION
		});

		function cancelar(btn, l, msg) {
			console.log('cancela');
			if (btn === 'ok') {
				win.close();
			}
		};
	},
	
	guardarWinDefinicionTiempos: function() {
		var win, me = this;
		win = me.getWinDefinicionTiempos();
		
		win.close();
	},
	
	verDetalleServicio: function (){
		console.log('iniciando BusquedaServiciosNavieros');
		var grilla, panel, me = this;
		grilla = me.getBsn().getComponent('fieldsetResultadoBuesqueda').getComponent('gridpanelResultadoBusqueda');
		if (grilla.getSelectionModel().getSelection().length === 0) {
			Ext.MessageBox.show({
				title: 'Modificar.',
				msg: 'Debe seleccionar un elemento de la grilla.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			});
			return;
		}
		var win = Ext.create('Exe.silcomex.view.aventuraMaritima.DetalleServicioNaviero');
		win.show();
	}
	
});