/**
* 
*/
Ext.define('Exe.silcomex.controller.Exportacion', {
    extend: 'Ext.app.Controller',
    refs : [{
        ref: 'contenedoPrincipal',
        selector: '#panelContenedor'
    }],
     views: [
        'exportacion.InformacionGeneral',
        'exportacion.GridUnidadesTransporte',
        'exportacion.CondicionEspecialCarga',
        'exportacion.WinBuscarShipper'
    ],
    
    init: function(){
    	console.log('Iniciando controller Exportacion');
        this.control({
            'button[action=preasignarUndTransporte]': {
                click : this.preasignarUndTransporte
            },
            'button[action=verPreasignadoUndTransporte]': {
                click : this.verPreasignadoUndTransporte
            },
            'button[action=eliminarUndTransporte]': {
                click : this.eliminarUndTransporte
            },
            'button[action=modificarUndTransporte]': {
                click : this.modificarUndTransporte
            },
            'button[action=aceptarDCLCaragaContenerizada]': {
                click : this.aceptarDCLCaragaContenerizada
            },
            'button[action=cancelarDCLCaragaContenerizada]': {
                click : this.cancelarDCLCaragaContenerizada
            },
            'button[action=buscarShipper]': {
                click : this.buscarShipper
            },

        });
    	
    },

    preasignarUndTransporte: function(){
        console.log('preasignarUndTransporte');
    },

    verPreasignadoUndTransporte: function(){
        console.log('verPreasignadoUndTransporte');
    },

    eliminarUndTransporte: function(){
        console.log('eliminarUndTransporte');
    },

    modificarUndTransporte: function(){
        console.log('modificarUndTransporte');
    },

    agregarUndTransporte: function(){
        console.log('agregarUndTransporte');
    },

    aceptarDCLCaragaContenerizada: function(){
        console.log('aceptarDCLCaragaContenerizada');
    },

    cancelarDCLCaragaContenerizada: function(){
        console.log('cancelarDCLCaragaContenerizada');
    },
    buscarShipper: function(){
        console.log('buscarShipper');
        var w = Ext.create('Exe.silcomex.view.exportacion.WinBuscarShipper');
        w.show();

    }
});
