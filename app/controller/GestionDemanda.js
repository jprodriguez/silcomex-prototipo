/**
* 
*/
Ext.define('Exe.silcomex.controller.GestionDemanda', {
    extend: 'Ext.app.Controller',
    refs : [{
        ref: 'contenedoPrincipal',
        selector: '#panelContenedor'
    }],
    
    stores: [
        /*'emisionDocLogistico.TipoOperacionDocLogisticos',
        'emisionDocLogistico.TipoDocumentoIMO',
        'emisionDocLogistico.TipoEstadoDocumento',
        'emisionDocLogistico.storeDocumentosIMO',
        'emisionDocLogistico.storeShipperConsignorSender',
        'emisionDocLogistico.storeConsignee',
        */

    ],
    
    models: [
    /*
        'emisionDocLogistico.modelListaDocumentosIMO',
        'emisionDocLogistico.modelListaShipperConsignorSender',
        'emisionDocLogistico.modelListaConsignee',
        */
    ],
    
    views: [
    
        'gdm.MainGestionDemanda'
        

    ],

    init: function(){
        console.log('Iniciando controller GestionDemanda');
        this.control({
            'button[action=gestionDemanda]': {
                //click : this.initEmDocLogistico
            }
        });
    },

    initGestionDemanda: function(){
        console.log('click Gestion de la Demanda');
        this.getContenedoPrincipal().removeAll();
        this.getContenedoPrincipal().add({
            xtype: 'mainGestionDemanda'
        });
        this.getContenedoPrincipal().doLayout();
    }
});

