/*
 *
 */

Ext.define('Exe.silcomex.store.aventuraMaritima.CriterioBusqueda', {
    extend: 'Ext.data.Store',

    requires: [
        'Exe.silcomex.model.GLOBAL_Combo'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Exe.silcomex.model.GLOBAL_Combo',
            storeId: 'storeCriterioBusqueda',
            data: [{
                id: '1',
                nombre: 'Todos'
            },{
                id: '2',
                nombre: 'Nombre Ruta'
            },{
                id: '3',
                nombre: 'Etapa'
            }]
        }, cfg)]);
    }
});