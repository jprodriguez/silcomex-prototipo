/**
 * ToolBar contenedor del menu de la aplicacion
 */

Ext.define("Exe.silcomex.view.estructural.MenuToolBar", {
 	extend: 'Ext.toolbar.Toolbar',
	alias: 'widget.menuToolBar',
	xtype: 'basic-toolbar',
	frame: true,
	requieres:['Ext.menu.*'],
	
	/**
	* @constructor
	*/
	initComponent: function() {
		Ext.apply(this, {
			items: [{
				text:'Aventura Maritima',
				//iconCls: 'bmenu',
				menu: this.menuAventuraMaritima()
			},{
				xtype: 'tbseparator' 
			},{
				text: 'Explorar Ficheros',
				menu: this.menuExplorarFicheros()
			},{
				xtype: 'tbseparator' 
			},{
				text: 'Exportación',
				menu: this.menuExportacion()
			},{
				xtype: 'tbseparator' 
			},{
				text: 'Emisión Doc Logisticos',
				menu: this.menuDocLogisticos()
			},{
				xtype: 'tbseparator' 
			},{
				text: 'Gestión de la Demanda',
				menu: this.menuGestionDemanda()
			}]
		});
		this.callParent();
	},
	
	menuAventuraMaritima: function () {
		return Ext.create('Ext.menu.Menu', {
			items:[{
				text: 'Aventura Maritima',
				action: 'aventuraMaritima'
			},{
				text: 'Buscar Servicios',
				action: 'buscarServicios'
			}]
		});
	},
	
	menuExplorarFicheros: function () {
		return Ext.create('Ext.menu.Menu', {
			items:[{
				text: 'Explorar Ficheros',
				action: 'explorarFicheros' 
			}]
		});
	},
	
	menuExportacion: function() {
		return Ext.create('Ext.menu.Menu', {
			items:[{
				text: 'Registrar DCL Carga Contenerizada',
				action: 'registrarDCLCargaContenerzada'
			},{
				text: 'Registrar DCL Carga Fraccionada',
				action: 'registrarDCLCargaFraccionada' 
			}]
		});
	},
	
	menuDocLogisticos: function() {
		return Ext.create('Ext.menu.Menu', {
			items:[{
				text: 'Emisión Doc Logisticos',
				action: 'emDocLogisticos' 
			}]
		});
	},
	
	menuGestionDemanda: function() {
		return Ext.create('Ext.menu.Menu', {
			items:[{
				text: 'OOFF',
				action: 'gestionDemanda' 
			},{
				text: 'Deposito',
				action: 'gestionDemanda'
			},{
				text: 'Sizeal',
				action: 'gestionDemanda'
			}]
		});
	}
	
});