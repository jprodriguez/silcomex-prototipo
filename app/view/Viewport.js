/**
* Viewport principal, aloja e intercambia los componentes de la aplicacion
*/
Ext.define('Exe.silcomex.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires:['Ext.layout.container.Border'],

    layout: {
        type: 'border'
    },

    defaults: {
        xtype: 'panel',
        layout: 'fit',
        frame: true
    },

    items: [{
        region: 'north',
        dockedItems: [{
            html: '<div><img src="resources/img/fondo_header_silcomex_01.jpg" /></div>',
            dock: 'top'
        },{
            xtype: 'menuToolBar',
            dock: 'bottom'
        }]
    },{
        region: 'center',
        id: 'panelContenedor',
        itemId: 'panelContenedor',
        items:[{
            //xtype: 'buscadorPrincipal'
        }]
    },{
        region: 'south',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer'
        }]
    }]
});
