/*
 * File: app/view/winCrearRuta.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Exe.silcomex.view.aventuraMaritima.WinCrearRuta', {
    extend: 'Ext.window.Window',
	alias: 'widget.winCrearRuta',

    frame: true,
    width: 797,
    title: 'Crear Ruta',
	modal: true,
	itemId: 'winCrearRuta',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
			buttonAlign: 'left',
            buttons: [{
				text: 'Cancelar',
				action: 'cancelarWinAgregarRuta'
			},{
				text: 'Guardar',
				action: 'guardarWinAgregarRuta'
			}],
            items: [{
				xtype: 'form',
				itemId: 'formAgregarRuta',
				border: false,
				frame: true,
				style: {
					border: '0px'
				},
				defaults: {
					padding: '0px, 10px, 0px,0px'
				},
				layout: {
					type: 'column'
				},
				items: [{
					xtype: 'textfield',
					width: 200,
					fieldLabel: '* Nombre Ruta',
					labelAlign: 'top',
					name: 'nombre',
					allowBlank: false
				}, {
					xtype: 'textfield',
					width: 200,
					fieldLabel: '* Descripción',
					labelAlign: 'top',
					name: 'descripcion',
					allowBlank: false
				}, {
					xtype: 'combobox',
					width: 200,
					fieldLabel: '* Destinación',
					labelAlign: 'top',
					name: 'destinacion',
					displayField: 'nombre',
					queryMode: 'local',
					store: 'aventuraMaritima.Destinacion',
					valueField: 'id',
					allowBlank: false
				} ]
			}, {
				xtype: 'panel',
				buttons: [{
					text: 'Buscar'
				}],
				buttonAlign: 'left',
				border: false,
				frame: true,
				style: {
					border: '0px'
				},
				defaults: {
					padding: '0px, 10px, 0px,0px'
				},
				items: [{
					xtype: 'radiogroup',
					width: 400,
					fieldLabel: 'Filtrar por',
					labelAlign: 'top',
					items: [{
						xtype: 'radiofield',
						boxLabel: 'Puerto'
					},{
						xtype: 'radiofield',
						boxLabel: 'Región'
					}]
				}, {
					xtype: 'textfield',
					width: 200
				}]
			}, {
				xtype: 'panel',
				border: false,
				frame: true,
				style: {
					border: '0px'
				},
				layout: {
					type: 'column'
				},
				items: [{
					xtype: 'fieldset',
					frame: false,
					columnWidth: 0.5,
					title: 'Resultado Búsqueda',
					items: [{
						xtype: 'gridpanel',
						draggable: true,
						height: 200,
						forceFit: true,
						store: 'aventuraMaritima.ResultadoBusqueda',
						columns: [{
							xtype: 'rownumberer',
							dataIndex: 'string',
							text: 'N°'
						}, {
							xtype: 'gridcolumn',
							dataIndex: 'puerto',
							text: 'Puerto'
						}, {
							xtype: 'gridcolumn',
							dataIndex: 'region',
							text: 'Región'
						}],
						viewConfig: {
							plugins: [
								Ext.create('Ext.grid.plugin.DragDrop', {

								})
							]
						},
						dockedItems: [{
							xtype: 'pagingtoolbar',
							dock: 'bottom',
							width: 360,
							displayInfo: true,
							store: 'aventuraMaritima.ResultadoBusqueda'
						}]
					}]
				}, {
					xtype: 'fieldset',
					frame: false,
					columnWidth: 0.5,
					title: 'Puertos en Ruta',
					items: [{
						xtype: 'gridpanel',
						draggable: true,
						height: 200,
						forceFit: true,
						store: 'aventuraMaritima.PuertosRuta',
						columns: [{
							xtype: 'rownumberer',
							dataIndex: 'string',
							text: 'N°'
						},{
							xtype: 'gridcolumn',
							dataIndex: 'puerto',
							text: 'Puerto'
						},{
							xtype: 'gridcolumn',
							dataIndex: 'region',
							text: 'Región'
						}],
						viewConfig: {
							plugins: [
								Ext.create('Ext.grid.plugin.DragDrop', {

								})
							]
						},
						dockedItems: [{
							xtype: 'pagingtoolbar',
							dock: 'bottom',
							width: 360,
							displayInfo: true,
							store:'aventuraMaritima.PuertosRuta'
						}]
					}]
				}]
			}]
        });
        me.callParent(arguments);
    }

});