/*
 * File: app/view/cl/exe/silcomex/BusquedaServiciosNavieros.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Exe.silcomex.view.aventuraMaritima.BusquedaServiciosNavieros', {
    extend: 'Ext.panel.Panel',
	alias:'widget.busquedaServiciosNavieros',
    frame: true,
    title: 'Búsqueda Servicios Navieros',
	itemId: 'busquedaServiciosNavieros',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
			style: {
				border: '0px'
			},
            items: [{
				xtype: 'tabpanel',
				activeTab: 0,
				itemId: 'tabPanelBuscarServicioNaviero',
				border: false,
				style: {
					border: '0px'
				},
				items: [{
					xtype: 'panel',
					title: 'Importación',
					border: false,
					frame: true,
					style: {
						border: '0px'
					},
					items:[{
						xtype: 'form',
						bodyPadding: 10,
						buttonAlign: 'left',
						buttons: [{
							text: 'Cancelar'
						},{
							text: 'Buscar'
						}],
						border: false,
						frame: true,
						style: {
							border: '0px'
						},
						layout: {
							type: 'column'
						},
						items: [{
							xtype: 'fieldset',
							columnWidth: 0.25,
							frame: true,
							height: 125,
							title: 'Origen',
							items: [{
								xtype: 'combobox',
								anchor: '100%',
								width: 150,
								fieldLabel: 'Puerto',
								labelAlign: 'top',
								name: 'puertoOrigen',
								displayField: 'nombre',
								queryMode: 'local',
								store: 'aventuraMaritima.Puerto',
								valueField: 'id'
							}]
						}, {
							xtype: 'fieldset',
							columnWidth: 0.25,
							frame: true,
							height: 125,
							title: 'Destino',
							items: [{
								xtype: 'combobox',
								anchor: '100%',
								fieldLabel: 'Pais',
								labelAlign: 'top',
								name: 'paisDestino',
								displayField: 'nombre',
								queryMode: 'local',
								store: 'aventuraMaritima.Pais',
								valueField: 'id'
							}, {
								xtype: 'combobox',
								anchor: '100%',
								width: 150,
								fieldLabel: 'Puerto',
								labelAlign: 'top',
								name: 'puertoDestino',
								displayField: 'nombre',
								queryMode: 'local',
								store: 'aventuraMaritima.Puerto',
								valueField: 'id'
							}]
						}]
					}]
				}, {
					xtype: 'panel',
					title: 'Exportación',
					border: false,
					frame: true,
					style: {
						border: '0px'
					},
					items:[{
						xtype: 'form',
						bodyPadding: 10,
						buttonAlign: 'left',
						buttons: [{
							text: 'Cancelar'
						}, {
							text: 'Buscar'
						}],
						border: false,
						frame: true,
						style: {
							border: '0px'
						},
						layout: {
							type: 'column'
						},
						items: [{
							xtype: 'fieldset',
							columnWidth: 0.25,
							frame: true,
							height: 125,
							title: 'Destino',
							items: [{
								xtype: 'combobox',
								anchor: '100%',
								fieldLabel: 'Pais',
								labelAlign: 'top',
								name: 'paisDestino',
								displayField: 'nombre',
								queryMode: 'local',
								store: 'aventuraMaritima.Pais',
								valueField: 'id'
							}, {
								xtype: 'combobox',
								anchor: '100%',
								width: 150,
								fieldLabel: 'Puerto',
								labelAlign: 'top',
								name: 'puertoOrigen',
								displayField: 'nombre',
								queryMode: 'local',
								store: 'aventuraMaritima.Puerto',
								valueField: 'id'
							}]
						}, {
							xtype: 'fieldset',
							columnWidth: 0.25,
							frame: true,
							height: 125,
							title: 'Origen',
							items: [{
								xtype: 'combobox',
								anchor: '100%',
								width: 150,
								fieldLabel: 'Puerto',
								labelAlign: 'top',
								name: 'puertoDestino',
								displayField: 'nombre',
								queryMode: 'local',
								store: 'aventuraMaritima.Puerto',
								valueField: 'id'
							}]
						}]
					}]
				}, {
					xtype: 'panel',
					title: 'Crucero',
					border: false,
					frame: true,
					style: {
						border: '0px'
					},
					items:[{
						xtype: 'form',
						bodyPadding: 10,
						buttonAlign: 'left',
						buttons: [{
							text: 'Cancelar'
						}, {
							text: 'Buscar'
						}],
						border: false,
						frame: true,
						style: {
							border: '0px'
						},
						layout: {
							type: 'column'
						},
						items: [{
							xtype: 'fieldset',
							columnWidth: 0.25,
							frame: true,
							height: 75,
							layout: {
								type: 'auto'
							},
							title: 'Puerto de Recalada',
							items: [{
								xtype: 'combobox',
								frame: false,
								fieldLabel: 'Puerto',
								labelAlign: 'top',
								name: 'puerto',
								allowBlank: false,
								displayField: 'nombre',
								queryMode: 'local',
								store : 'aventuraMaritima.Puerto',
								valueField: 'id'
							}]
						}]
					}]
				}]
			}, {
				xtype: 'fieldset',
				title: 'Resultado de la Búsqueda',
				itemId: 'fieldsetResultadoBuesqueda',
				items: [{
					xtype: 'gridpanel',
					itemId: 'gridpanelResultadoBusqueda',
					height: 250,
					forceFit: true,
					store: 'aventuraMaritima.ResultadoServicios',
					columns: [{
						xtype: 'rownumberer',
						dataIndex: 'string',
						text: 'N°'
					}, {
						xtype: 'gridcolumn',
						width: 40,
						dataIndex: 'string',
						text: ''
					}, {
						xtype: 'gridcolumn',
						width: 130,
						dataIndex: 'nombre',
						text: 'Nombre del Servicio'
					}, {
						xtype: 'gridcolumn',
						width: 130,
						dataIndex: 'naviera',
						text: 'Naviera'
					}, {
						xtype: 'gridcolumn',
						width: 130,
						dataIndex: 'agencia',
						text: 'Agencia '
					}, {
						xtype: 'gridcolumn',
						width: 90,
						dataIndex: 'duracion',
						text: 'Tiempo de Ciclo'
					}, {
						xtype: 'gridcolumn',
						width: 90,
						dataIndex: 'tipoServicio',
						text: 'Tipo Servicio'
					}, {
						xtype: 'numbercolumn',
						width: 120,
						dataIndex: 'frecuencia',
						text: 'Frecuencia del Servicio',
						format: '0,000'
					}, {
						xtype: 'numbercolumn',
						width: 75,
						dataIndex: 'transit',
						text: 'Transit Time'
					}],
					dockedItems: [{
						xtype: 'pagingtoolbar',
						dock: 'bottom',
						width: 360,
						displayInfo: true,
						store: 'aventuraMaritima.ResultadoServicios'
					}, {
						xtype: 'toolbar',
						dock: 'top',
						items: [{
							text: 'Ver detalle',
							action: 'verDetalleServicio'
						}]
					}]
				}]
			}]
        });

        me.callParent(arguments);
    }

});