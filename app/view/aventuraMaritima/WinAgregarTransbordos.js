/*
 * File: app/view/cl/exe/silcomex/WinAgregartransbordos.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Exe.silcomex.view.aventuraMaritima.WinAgregarTransbordos', {
    extend: 'Ext.window.Window',
	alias: 'widget.winAgregarTransbordos',

    buttonAlign: 'left',
    frame: true,
    width: 600,
    title: 'Agregar Transbordos',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            buttons: [{
				text: 'Cancelar'
			}, {
				text: 'Guardar'
			}],
            items: [{
				xtype: 'panel',
				buttonAlign: 'left',
				buttons: [{
					text: 'Agregar'
				}],
				border: false,
				frame: true,
				style: {
					border: '0px'
				},
				items: [{
					xtype: 'combobox',
					fieldLabel: '* Rutas',
					labelAlign: 'top',
					name: 'rutas',
					displayField: 'nombre',
					queryMode: 'local',
					store: 'aventuraMaritima.Rutas',
					valueField: 'id'
				}]
			}, {
				xtype: 'gridpanel',
				tbar: [{
					text: 'Eliminar'
				}],
				height: 250,
				forceFit: true,
				store: 'aventuraMaritima.TiemposEntrePuertos',
				columns: [{
					xtype: 'rownumberer',
					dataIndex: 'string',
					text: 'N°'
				}, {
					xtype: 'gridcolumn',
					width: 200,
					dataIndex: 'ruta',
					text: 'Nombre Ruta'
				}, {
					xtype: 'gridcolumn',
					width: 200,
					dataIndex: 'puerto',
					text: 'Puerto Transbordo'
				}],
				dockedItems: [{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					width: 360,
					displayInfo: true,
					store: 'aventuraMaritima.TiemposEntrePuertos'
				}]
			}]
        });
        me.callParent(arguments);
    }

});