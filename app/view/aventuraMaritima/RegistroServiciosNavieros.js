/**
*
*/
Ext.define('Exe.silcomex.view.aventuraMaritima.RegistroServiciosNavieros', {
    extend: 'Ext.panel.Panel',
	alias:'widget.registroServiciosNavieros',
	requires:['Ext.grid.Panel','Ext.grid.column.RowNumberer','Ext.form.FieldSet',
		'Ext.layout.container.Column','Ext.form.field.ComboBox', 'Ext.grid.plugin.DragDrop',
		'Exe.silcomex.view.aventuraMaritima.NuevoRegistroServiciosNavieros', 'Ext.tab.Panel'],
    frame: true,
    width: 759,
    title: 'Registro Servicio Navieros',
	itemId: 'registroServiciosNavieros',

   	/**
    * @constructor
    */
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [{
				xtype: 'fieldset',
				title: 'Filtro de Búsqueda',
				items: [{
					xtype: 'form',
					bodyPadding: 10,
					buttons: [{
						text: 'Buscar'
					}],
					border: false,
					frame: true,
					style: {
						border: '0px'
					},
					defaults: {
						padding: '0px, 10px, 0px, 0px'
					},
					layout: {
						type: 'column'
					},
					items: [{
						xtype: 'combobox',
						width: 200,
						fieldLabel: 'Criterio Búsqueda',
						labelAlign: 'top',
						displayField: 'nombre',
						queryMode: 'local',
						store: 'storeCriterio',
						valueField: 'id'
					}, {
						xtype: 'textfield',
						width: 200,
						fieldLabel: ' ',
						labelAlign: 'top',
						labelSeparator: '&nbsp;'
					}]
				}]
			},  {
				xtype: 'fieldset',
				title: 'Resultado Búsqueda',
				itemId: 'fresultado',
				items: [{
					itemId: 'grillaResultadoServicios',
					xtype: 'gridpanel',
					height: 250,
					forceFit: true,
					store: 'storeServicioNavieros',
					columns: [{
						xtype: 'rownumberer',
						dataIndex: 'string',
						text: 'N°'
					},  {
						xtype: 'gridcolumn',
						dataIndex: 'nombre',
						text: 'Nombre Servicio'
					}, {
						xtype: 'gridcolumn',
						dataIndex: 'tipo',
						text: 'Tipo Servicio'
					}, {
						xtype: 'gridcolumn',
						dataIndex: 'descripcion',
						text: 'Descripción del Servicio'
					}, {
						xtype: 'gridcolumn',
						dataIndex: 'frecuencia',
						text: 'Frecuencia del Servicio'
					}, {
						xtype: 'gridcolumn',
						dataIndex: 'duracion',
						text: 'Duración del Servicio'
					} ],
					dockedItems: [{
						xtype: 'pagingtoolbar',
						dock: 'bottom',
						width: 360,
						displayInfo: true
					},{
						xtype: 'toolbar',
						dock: 'top',
						items: [{
							xtype: 'button',
							text: 'Agregar',
							action: 'agregarServicio',
							scope : this
						},{
							xtype: 'tbseparator'
						},{
							xtype: 'button',
							text: 'Modificar',
							action: 'modificarServicio',
							scope : this
						},{
							xtype: 'tbseparator'
						}]
					}]
				}]
			}]
        });

        me.callParent(arguments);
    }

});