/*
 *
 */

Ext.define('Exe.silcomex.view.aventuraMaritima.WinDefinicionTiempos', {
    extend: 'Ext.window.Window',
    alias: 'widget.winDefinicionTiempos',
	requires:['Ext.grid.plugin.RowEditing', 'Ext.grid.column.Number', 'Ext.grid.feature.GroupingSummary'],

    frame: true,
    width: 627,
    title: 'Definición de Tiempos',
	modal: true,
	itemId: 'winDefinicionTiempos',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            buttons: [{
				text: 'Cancelar',
				action: 'cancelarWinDefinicionTiempos'
			}, {
				text: 'Guardar',
				action: 'guardarWinDefinicionTiempos'
			}],
            items: [{
				xtype: 'fieldset',
				frame: true,
				title: 'Tiempos Entre Puertos',
				items: [{
					xtype: 'gridpanel',
					height: 250,
					forceFit: true,
					store: 'aventuraMaritima.TiemposEntrePuertos',
					columns: [{
						xtype: 'rownumberer',
						dataIndex: 'number',
						text: 'N°'
					}, {
						xtype: 'gridcolumn',
						dataIndex: 'ruta',
						text: 'Ruta'
					}, {
						xtype: 'gridcolumn',
						dataIndex: 'origen',
						text: 'Origen'
					}, {
						xtype: 'gridcolumn',
						dataIndex: 'destino',
						text: 'Destino'
					}, {
						xtype: 'numbercolumn',
						summaryRenderer: function(val, params, data) {
							return Ext.String.format('{0} Total', val);
						},
						summaryType: 'sum',
						dataIndex: 'tiempo',
						text: 'Tiempo (Días)',
						editor: {
							xtype: 'numberfield'
						}
					}],
					plugins: [
						Ext.create('Ext.grid.plugin.RowEditing', {

						})
					],
					dockedItems: [{
						xtype: 'pagingtoolbar',
						dock: 'bottom',
						width: 360,
						displayInfo: true,
						store: 'aventuraMaritima.TiemposEntrePuertos'
					}],
					features: [{
						ftype: 'groupingsummary',
						groupHeaderTpl: [
							'Nombre Ruta :{name}',
						]
					}]
				}]
			}]
        });

        me.callParent(arguments);
    }

});