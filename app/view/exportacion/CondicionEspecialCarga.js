/**
 * 
 */
Ext.define('Exe.silcomex.view.exportacion.CondicionEspecialCarga', {
    extend: 'Ext.form.Panel',
    alias : 'widget.condicionEspecialCarga',

    frame: false,
    border: false,
    bodyStyle: 'padding:5px ',
    layout: 'column',
    elementos: null,

    /**
    * @constructor
    */
    initComponent: function() {
        var me = this;
        me.iniciarItems();
        Ext.applyIf(me, {
            items: me.elementos
        });
        me.callParent(arguments);
    },
    
    iniciarItems: function(){
        this.elementos = [{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Tipo de Carga',
                name: 'tipoCarga'
            }, {
                fieldLabel: 'Peso Bruto',
                name: 'pesoBruto'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Descripci\u00f3n de la carga',
                name: 'descripcionCarga'
            }, {
                fieldLabel: 'Volumen Bruto',
                name: 'volumenBruto'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top', width: 180},
            items: [{
                fieldLabel: 'HS Code',
                name: 'HSCode',
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    flex: 5
                }, {
                    xtype: 'splitter'
                }, {
                    xtype: 'button',
                    glyph: 72
                }]
            }, {
                fieldLabel: 'IMO',
                name: 'imo'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Sobredimensionado',
                name: 'sobredimensionado'
            }, {
                xtype: 'combobox',
                fieldLabel: 'Naviera',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Nº Contenedor',
                name: 'numContenedor'
            }, {
                fieldLabel: 'Nave',
                name: 'nave'
            }]
        }];
    }
});