/**
* 
*/
Ext.define("Exe.silcomex.view.exportacion.MainExportacion", {
    extend: 'Ext.panel.Panel',
    alias : 'widget.mainExportacion',
    title: 'Exportacion',
    bodyStyle: 'padding:5px ',
    autoScroll: true,
    buttonAlign: 'left',

    /**
    * @constructor
    */
    initComponent: function() {
    	console.log('iniciando main exportacion');
        var me = this;
        me.items = me.getItems();
        me.buttons = me.getButtons();

        me.callParent(arguments);
    },

    getItems: function(){
        return [{
            xtype: 'fieldset',
            title: 'Informaci\u00f3n General',
            collapsible: true,
            width: '100%',
            items :[{
                xtype: 'informacionGeneral'
            }]
        },{
            xtype: 'fieldset',
            title: 'Informaci\u00f3n Unidades de Transporte',
            collapsible: true,
            width: '100%',
            items :[{
                xtype: 'gridUnidadesTransporte'
            }]
        },{
            xtype: 'fieldset',
            title: 'Condici\u00f3n Especial de Carga',
            collapsible: true,
            width: '100%',
            items :[{
                xtype: 'condicionEspecialCarga'
            }]
        }]
    },

    getButtons: function(){
        return [{
            text: 'Aceptar',
            action: 'aceptarDCLCaragaContenerizada'
        },{
            text: 'Cancelar',
            action: 'cancelarDCLCaragaContenerizada'
        }]
    }

});