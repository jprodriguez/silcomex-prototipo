/**
 * 
 */
Ext.define('Exe.silcomex.view.exportacion.GridUnidadesTransporte', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.gridUnidadesTransporte',

    frame: false,
    border: false,
    bodyStyle: 'padding:5px 5px 0',
    columnas: null,
    menu: null,
    forceFit: true,

    /**
    * @constructor
    */
    initComponent: function() {
        var me = this;
        me.initToolBar();
        me.iniColumnas();
        me.columns = me.columnas;
        me.dockedItems = me.menu;
        me.callParent(arguments);
    },

    iniColumnas: function(){
        this.columnas = [
            {text: 'Tipo Und. Trans.', dataIndex: 'fichero'},
            {text: 'Cantidad', dataIndex: 'cantidad'},
            {text: 'Fecha de Vacio', dataIndex: 'fachaVacio'},
            {text: 'Temperatura Referencial', dataIndex: 'temperaturaReferencial'},
            {text: 'Flujo de Aire', dataIndex: 'flujoAire'},
            {text: 'Humedad(%)', dataIndex: 'humedad'},
            {text: 'Opciones de Ventilación', dataIndex: 'opcVentilacion'},
            {text: 'Humidificación', dataIndex: 'humidificacion'},
            {text: 'GenSet', dataIndex: 'genSet'},
            {text: 'Atmósfera Controlada', dataIndex: 'atmosferaControlada'},
            {text: 'Preasignado', dataIndex: 'preasignado'}
        ];
    },

    initToolBar : function(){
        this.menu = [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Preasignar', action: 'preasignarUndTransporte'
            },{
                text: 'Ver Preasignado', action: 'verPreasignadoUndTransporte'
            },{
                text: 'Eliminar', action: 'eliminarUndTransporte'
            },{
                text: 'Modificar', action: 'modificarUndTransporte'
            },{
                text: 'Agragar Und. Transporte', action: 'agregarUndTransporte'
            }]
        }]
    }
});