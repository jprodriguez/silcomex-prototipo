/*
 *
 */
Ext.define('Exe.silcomex.view.exportacion.WinBuscarShipper', {
    extend: 'Ext.window.Window',
	alias:'widget.winBuscarShipper',

    buttonAlign: 'left',
    frame: false,
	itemId: 'winBuscarShipper',
	
    title: 'Buscar Shipper',
    modal: true,
    botones: null,

    initComponent: function() {
        var me = this;
        me.initButtons();

        me.callParent(arguments);
    },

    initform: function(){
    	/*buttons: [{
				text: 'Cancelar',
				action: 'cancelarWinAgregarNaviera'
			}, {
				text: 'Guardar',
				action: 'guardarWinAgregarNaviera'
			}],
            items: [{
				xtype: 'form',
				itemId: 'formAgregarNaviera',
				border: false,
				frame: true,
				style: {
					border: '0px'
				},
				defaults: {
					labelAlign: 'top',
					padding: '0px, 10px, 0px, 10px',
				},
				items: [{
					xtype: 'combobox',
					width: 200,
					fieldLabel: '* buscar Naviera',
					name: 'naviera',
					displayField: 'nombre',
					queryMode: 'local',
					store: 'aventuraMaritima.Navieras',
					valueField: 'id',
					allowBlank: false
				}, {
					xtype: 'textfield',
					width: 200,
					fieldLabel: '* Nombre Servicio de la Naviera',
					name: 'nombre',
					allowBlank: false
				}]
			}]*/
    },

    initButtons: function(){
    	this.botones = [{
			text: 'Cancelar',
			action: 'cancelarWinAgregarNaviera'
		}, {
			text: 'Guardar',
			action: 'guardarWinAgregarNaviera'
    	}]
    }

});