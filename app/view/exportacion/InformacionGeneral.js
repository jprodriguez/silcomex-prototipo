/**
 * 
 */
Ext.define('Exe.silcomex.view.exportacion.InformacionGeneral', {
    extend: 'Ext.form.Panel',
    alias : 'widget.informacionGeneral',

    frame: false,
    border: false,
    bodyStyle: 'padding:5px ',
    layout: 'column',
    elementos: null,

    /**
    * @constructor
    */
    initComponent: function() {
        var me = this;
        me.iniciarItems();
        Ext.applyIf(me, {
            items: me.elementos
        });
        me.callParent(arguments);
    },
    
    iniciarItems: function(){
        this.elementos = [{
            columnWidth: .2,
            border : false,
            defaults: {anchor: '100%', labelAlign: 'top'},
            items: [{
                xtype: 'combobox',
                fieldLabel: '* Número de Booking',
                name: 'numBooking',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id',
                allowBlank: false
            }, {
                xtype: 'combobox',
                fieldLabel: '* Nave',
                name: 'nave',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id',
                allowBlank: false

            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Emisor Booking',
                name: 'emisorBooking',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id',
                allowBlank: false
            }, {
                fieldLabel: 'Número de Viaje',
                name: 'numViaje'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top', width: 180},
            items: [{
                fieldLabel: '* Shipper',
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    name: 'shipper',
                    allowBlank: false,
                    flex: 5
                }, {
                    xtype: 'splitter'
                }, {
                    xtype: 'button',
                    action: 'buscarShipper',
                    glyph: 72
                }]
            }, {
                xtype: 'combobox',
                fieldLabel: 'Puerto Origen',
                name: 'puertoOrigen',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id',
                allowBlank: false
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top', width: 180},
            items: [{
                fieldLabel: 'Rut Shipper',
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    name: 'rutShipper',
                    allowBlank: false,
                    flex: 5
                }, {
                    xtype: 'splitter'
                }, {
                    xtype: 'button',
                    action: 'buscarShipper',
                    glyph: 72
                }]
            }, {
                xtype: 'combobox',
                fieldLabel: 'Puerto Destino',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id',
                allowBlank: false
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Forwarder',
                name: 'numContenedor'
            }, {
                xtype: 'combobox',
                fieldLabel: 'Terminal de Embarque',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id',
                allowBlank: false
            }]
        }];
    }
});