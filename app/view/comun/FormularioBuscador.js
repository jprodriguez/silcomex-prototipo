/**
 * 
 */
Ext.define('Exe.silcomex.view.comun.FormularioBuscador', {
    extend: 'Ext.form.Panel',
    alias : 'widget.formBuscadorPrincipal',

    frame: false,
    border: false,
    bodyStyle: 'padding:5px 5px 0',
    layout: 'column',
    elementos: null,

    /**
    * @constructor
    */
    initComponent: function() {
        var me = this;
        me.iniciarItems();
        Ext.applyIf(me, {
            items: me.elementos
        });
        me.callParent(arguments);
    },
    
    iniciarItems: function(){
        this.elementos = [{
            columnWidth: .2,
            border : false,
            defaults: {anchor: '100%', labelAlign: 'top'},
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Operacion',
                name: 'operacion',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id'
            }, {
                xtype: 'combobox',
                fieldLabel: 'Tipo Documento',
                name: 'tipoDocumento',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id'

            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Nº Fichero',
                name: 'numFichero'
            }, {
                fieldLabel: 'Nº Documento',
                name: 'numDocumento'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Nº Brooking/BL',
                name: 'field1'
            }, {
                fieldLabel: 'wqeqe',
                name: 'field2'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Nº Envío',
                name: 'field1'
            }, {
                xtype: 'combobox',
                fieldLabel: 'Naviera',
                labelAlign: 'top',
                displayField: 'nombre',
                queryMode: 'local',
                store: 'storeCriterio',
                valueField: 'id'
            }]
        },{
            columnWidth: .2,
            border : false,
            defaults: {xtype: 'textfield', anchor: '100%', labelAlign: 'top'},
            items: [{
                fieldLabel: 'Nº Contenedor',
                name: 'numContenedor'
            }, {
                fieldLabel: 'Nave',
                name: 'nave'
            }]
        }];
    }
});