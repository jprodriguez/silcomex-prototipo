/**
 * 
 */
Ext.define('Exe.silcomex.view.comun.GridFicheros', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.gridFicheros',

    frame: false,
    border: false,
    bodyStyle: 'padding:5px 5px 0',
    columnas: null,
    menu: null,

    /**
    * @constructor
    */
    initComponent: function() {
        var me = this;
        me.initToolBar();
        me.iniColumnas();
        me.columns = me.columnas;
        me.dockedItems = me.menu;
        me.callParent(arguments);
    },

    iniColumnas: function(){
        this.columnas = [
            {text: 'Tipo', dataIndex: 'tipo'},
            {text: 'Tipo Carga', dataIndex: 'tipoCarga'},
            {text: 'Fichero', dataIndex: 'fichero'},
            {text: 'Booking/BL', dataIndex: 'bookinBl'},
            {text: 'Cliente', dataIndex: 'cliente'},
            {text: 'Naviera', dataIndex: 'naviera'},
            {text: 'Nave', dataIndex: 'nave'},
            {text: 'Nº Viaje', dataIndex: 'numViaje'},
            {text: 'P. Embarque', dataIndex: 'puertoEmbarque'},
            {text: 'P. Destino', dataIndex: 'puertoDestino'},
            {text: 'Estado', dataIndex: 'estado'},
            {text: 'F. Ult. Modificación', dataIndex: 'fecUltimamodificacion'},
            {text: 'Modificado por', dataIndex: 'modificadoPor'}
        ];
    },

    initToolBar : function(){
        this.menu = [{
            xtype: 'toolbar',
            dock: 'top',
            items: [
                { xtype: 'button', text: 'Button 1' }
            ]
        }]
    }
});