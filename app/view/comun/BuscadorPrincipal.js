/**
 * Cumple la funcion de buscador para todos los modulos y se carga al inicio.
 */
Ext.define('Exe.silcomex.view.comun.BuscadorPrincipal', {
    extend: 'Ext.form.Panel',
    alias : 'widget.buscadorPrincipal',

    frame: true,
    border: false,
    bodyStyle: 'padding:5px 5px 0',

    /**
    * @constructor
    */
    initComponent: function() {
        var me = this;
        me.items = me.getItems();
        me.callParent(arguments);
    },

    getItems: function(){
        return [{
            xtype: 'fieldset',
            title: 'Filtros de Busqueda',
            collapsible: true,
            width: '100%',
            items :[{
                xtype: 'formBuscadorPrincipal',
                buttonAlign: 'left',
                buttons: [{
                    text: 'Buscar'
                },{
                    text: 'Limpiar'
                }]
            }]
        },{
            xtype: 'fieldset',
            title: 'Filtros de Busqueda',
            collapsible: true,
            width: '100%',
            items :[{
                xtype: 'gridFicheros'
            }]
        }]
    }
});