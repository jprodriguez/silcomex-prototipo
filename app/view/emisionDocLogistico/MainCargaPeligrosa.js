/**
* 
*/
Ext.define("Exe.silcomex.view.emisionDocLogistico.MainCargaPeligrosa", {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.ux.RowExpander'
    ],    
    alias : 'widget.mainCargaPeligrosa',
    title: 'Carga Peligrosa',
    ref:'cargaPeligrosa',
    /**
    * @constructor
    */
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'fieldset',
                title: 'Filtros de Búsqueda',
                items: [
                        {
                        xtype: 'form',
                        columnWidth: 0.16,
                        border: false,
                        frame: true,
                        anchor:'100%',
                        style: {
                            border: '0px'
                        },
                        defaults: {
                            padding: '0px, 10px, 0px, 0px'
                        },
                        layout: {
                            type: 'column'
                        },
                        //width: 1200,
                        bodyPadding: 10,
                        frameHeader: false,
                        /*layout: {
                            type: 'table',
                            columns:6
                        },*/
                        items: [  
                                {
                                    xtype: 'combobox',
                                    id:'comboOperacion',
                                    //anchor: '100%',
                                    //width:160,
                                    fieldLabel: 'Operación',
                                    labelAlign: 'top',
                                    name: 'tipoOperacion',
                                    allowBlank: false,
                                    displayField: 'nombre',
                                    queryMode: 'local',
                                    store: 'emisionDocLogistico.TipoOperacionDocLogisticos',
                                    valueField: 'id'
                                },
                                {
                                    xtype: 'combobox',
                                    //width:160,
                                    //anchor: '100%',
                                    fieldLabel: 'Tipo Documento',
                                    labelAlign: 'top',
                                    name: 'tipoDocumentoIMO',
                                    //allowBlank: false,
                                    displayField: 'nombre',
                                    queryMode: 'local',
                                    store: 'emisionDocLogistico.TipoDocumentoIMO',
                                    valueField: 'id',
                                    emptyText:'Todos'
                                },
                                {
                                    xtype:'textfield',
                                    labelAlign: 'top',
                                    fieldLabel:'N°',
                                    name:'numCargaPeligrosa',
                                    //width:160
                                },
                                {
                                    xtype: 'combobox',
                                   // width:160,
                                    //anchor: '100%',
                                    fieldLabel: 'Estado',
                                    labelAlign: 'top',
                                    name: 'estadoDocumentoIMO',
                                    //allowBlank: false,
                                    displayField: 'nombre',
                                    queryMode: 'local',
                                    store: 'emisionDocLogistico.TipoEstadoDocumento',
                                    valueField: 'id',
                                    emptyText:'Todos'
                                },
                                {
                                    xtype:'textfield',
                                    labelAlign: 'top',
                                    fieldLabel:'N° Booking /',
                                    name:'numBookingCargaIMO',
                                    //width:160
                                },
                                {
                                    xtype:'textfield',
                                    labelAlign: 'top',
                                    fieldLabel:'Nave',
                                    name:'naveCargaIMO',
                                    //width:160
                                }
                        ]},
                        {
                        xtype: 'form',
                        columnWidth: 0.16,
                        border: false,
                        frame: true,
                        style: {
                            border: '0px'
                        },
                        defaults: {
                            padding: '0px, 10px, 0px, 0px'
                        },
                        layout: {
                            type: 'column'
                        },
                        //width: 1200,
                        bodyPadding: 10,
                        frameHeader: false,
                        /*layout: {
                            type: 'table',
                            columns:6
                        },*/
                        items: [          
                                {
                                    xtype:'textfield',
                                    labelAlign: 'top',
                                    fieldLabel:'N° Contenedor',
                                    name:'numContenedorCargaIMO',
                                   // width:160
                                },
                                {
                                    xtype:'datefield',
                                    labelAlign: 'top',
                                    fieldLabel:'Desde',
                                    name:'fechaDesdeCargaIMO',
                                   // width:120
                                },
                                {
                                    xtype:'datefield',
                                    labelAlign: 'top',
                                    fieldLabel:'Hasta',
                                    name:'fechaHastaCargaIMO',
                                    //width:120
                                }
                         ],
                        buttonAlign: 'left',
                        buttons: [{
                            text: 'Buscar'
                        },{
                            text: 'Limpiar'
                        }],
                }]
            },
            {
                xtype:'fieldset',
                title:'Documentos IMO',
                items:[
                       {xtype:'panel',
                        border: false,
                        frame: true,
                        style: {
                            border: '0px'
                        },
                        items: [ {
                            xtype: 'gridpanel',
                            autoScroll : true,
                            height:'100%',
                            tbar: [ {
                                    text: 'Crear',
                                    action: 'selTipoDocIMO'
                                }, '-', {
                                    text: 'Modificar',
                                }, '-', {
                                    text: 'Eliminar',
                                }],
                            forceFit: true,
                            store: 'emisionDocLogistico.storeDocumentosIMO',
                            columns: [ {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'operacion',
                                    text: 'Operacion'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'numero',
                                    text: 'N° Documento IMO'
                                },
                                  {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'tipo',
                                    text: 'Tipo'
                                }, {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'estado',
                                    text: 'Estado'
                                }, {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'fecha',
                                    text: 'Fecha del Documento'
                                }, {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'nave',
                                    text: 'Nave'
                                }, {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'numBookingBL',
                                    text: 'N° Booking o B/L'
                                }, {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'contenedor',
                                    text: 'Contenedores',
                                    renderer:renderIcon
                                } ],
                                plugins: [{
                                    ptype: 'rowexpander',
                                    rowBodyTpl : [
                                        '<p><b>Contenedores:</b> {detalleContenedor}</p>'
                                    ]
                                }],
                                collapsible: false,
                                animCollapse: false,

                            dockedItems: [ {
                                    xtype: 'pagingtoolbar',
                                    store: 'emisionDocLogistico.storeDocumentosIMO',
                                    dock: 'bottom',
                                    width: 360,
                                    displayInfo: true
                                } ]
                        } ],
                        buttonAlign: 'left',
                        buttons: [{
                            text: 'Imprimir'
                        },{
                            text: 'Exportar'
                        }] 
                    }
                ]  
            }
            ]
        });

        me.callParent(arguments);
    }

});
function renderIcon(val) {
    return '<img src="resources/img/desplegar.GIF"><label>EGHU 310819-9</label>';
}