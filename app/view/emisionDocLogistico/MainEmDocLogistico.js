/**
* 
*/
Ext.define("Exe.silcomex.view.emisionDocLogistico.MainEmDocLogistico", {
    extend: 'Ext.tab.Panel',
    alias : 'widget.mainEmDocLogistico',
    title: 'Emision de Documentos Logisticos',
    requires:['Ext.form.field.Date'],
    /**
    * @constructor
    */
   initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'mainCargaPeligrosa',
                    autoScroll : true,
                    height:'100%',
                },
                {
                    xtype: 'panel',
                    title: 'Carta Temperatura'
                },
                {
                    xtype: 'panel',
                    title: 'Matriz B/L'
                }
            ]
        });

        me.callParent(arguments);
    }

});