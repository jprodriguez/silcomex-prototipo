/**
+ Aplicacion
*/

Ext.define('Exe.silcomex.Application', {
    name: 'Exe.silcomex',

    extend: 'Ext.app.Application',

    views: [
        'comun.BuscadorPrincipal',
        'comun.FormularioBuscador',
        'comun.GridFicheros',
        'estructural.MenuToolBar'
    ],

    controllers: [
        'Main',
        'AventuraMaritima',
        'ExplorarFicheros',
        'Exportacion',
        'EmisionDocLogistico',
        'GestionDemanda'
    ],

    stores: [
        //nomenclatura definicion nombre stores
        //GLOBAL_storeComunas
		
    ],

    models: [
        //nomenclatura definicion nombre models
        //GLOBAL_modelComunas
		'GLOBAL_Combo'
    ]
});
