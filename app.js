

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides
/**
*
*/
Ext.application({
    name: 'Exe.silcomex',

    extend: 'Exe.silcomex.Application',
    
    autoCreateViewport: true
});
